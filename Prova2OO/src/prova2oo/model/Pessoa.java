package prova2oo.model;

public class Pessoa {
    private String nome;
    private String cpf;
    private String telefone;
    private String reinvidicacaoCodigo;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getReinvidicacaoCodigo() {
        return reinvidicacaoCodigo;
    }

    public void setReinvidicacao(String reinvidicacaoCodigo) {
        this.reinvidicacaoCodigo = reinvidicacaoCodigo;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    
}
