package prova2oo.model;

public class Reinvidicacao {
    private String codigoReinvidicacao;
    private String reinvidicacao;

    public String getCodigoReinvidicacao() {
        return codigoReinvidicacao;
    }

    public String getReinvidicacao() {
        return reinvidicacao;
    }
    
    public void setMensagemReinvidicacao(String codigoReinvidicacao, String reindivicacao){
        this.codigoReinvidicacao = codigoReinvidicacao;
        this.reinvidicacao = reinvidicacao;
    }
    
    
}
